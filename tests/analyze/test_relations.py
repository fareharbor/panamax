import mock
import pytest

from panamax.analyze.relations import get_tables_related_to, follow_relations
from panamax import Table

from tests.conftest import patched_format_sql, run_sql_query


@pytest.fixture(scope='module', autouse=True)
def patch_format_sql():
    with mock.patch('panamax.analyze.relations.db.format_sql', patched_format_sql):
        yield


def test_get_tables_related_to():
    # check that get_tables_related_to returns a dictionary
    # with table and column names with FK to the original table
    table_name = 'users'
    column = 'id'
    mocked_connection = mock.Mock()
    mocked_cursor = mock.Mock()
    mocked_connection.cursor.return_value = mocked_cursor

    exp_related_tables = {
        'cars': 'user_id',
        'email': 'u_id',
    }

    mocked_cursor.fetchall.return_value = exp_related_tables

    related_tables = get_tables_related_to(mocked_connection, table_name, column)

    exp_query = '''
    SELECT tc.table_name,
           kcu.column_name
    FROM information_schema.table_constraints tc
       JOIN information_schema.key_column_usage kcu ON tc.constraint_name = kcu.constraint_name
       JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_name = tc.constraint_name
    WHERE constraint_type = 'FOREIGN KEY'
       AND ccu.table_name=%s
       AND ccu.column_name=%s;
    '''

    mocked_cursor.execute.assert_called_once_with(exp_query, (table_name, column))

    assert related_tables == exp_related_tables


def test_follow_relations_simple_referencing_fk():
    # test that follow_relations returns a list of tables
    #  with relations when there is one related table (FK)
    mocked_connection = mock.Mock()

    def _get_related(_, table_name, column):
        relations = {
            'users': [{'table_name': 'cars', 'column_name': 'user_id'}],
        }
        return relations.get(table_name, {})

    # pass two tables. One of them has a related table
    # in the database: cars.user_id -> users
    # follow_relations should return a list
    # with original tables and the related `cars`
    users = Table('users', follow_relations=True)
    tables = [users, Table('companies')]

    with mock.patch('panamax.analyze.relations.get_tables_related_to', _get_related):
        with mock.patch('panamax.analyze.relations.get_fk_tables', return_value=[]):
            tables_with_relations = tables + follow_relations(mocked_connection, tables)

    # follow_relations should add a new table: `cars`
    # and build a query to select all related rows from it
    exp_cars_query = (
        'WITH ids AS (%s) SELECT cars.* FROM cars WHERE cars.user_id IN (SELECT id FROM ids)'
        % users.query
    )
    cars = Table('cars', query=exp_cars_query)

    assert sorted(tables_with_relations) == sorted(tables + [cars])


def test_follow_relations_should_add_referencing_tables(connection):
    # create tables:
    # bank_accounts
    #      ^
    #      |  FK bank_account_id
    #   companies
    #       |
    #       |-departments (company_id)
    #          |
    #          |-users (department_id)
    #          |-desks (user_id, company_id)
    #
    # and then create a manifest only with one table:
    # companies but with follow_relations=True.
    # `follow_relations` should find bank_accounts, departments, users ans desks
    # and return them as related tables.
    # desks has a direct FK to companies, so it should be fetched without joins.

    # create test data

    # companies
    query = 'CREATE TABLE bank_accounts (id INTEGER PRIMARY KEY, name varchar(100))'
    run_sql_query(connection, query)

    # companies
    query = '''
        CREATE TABLE companies (
            id INTEGER PRIMARY KEY,
            name varchar(100),
            bank_account_id INTEGER NOT NULL REFERENCES bank_accounts(id) DEFERRABLE
        )
    '''
    run_sql_query(connection, query)

    # departments
    query = '''
        CREATE TABLE departments (
            id INTEGER PRIMARY KEY,
            name varchar(100),
            company_id INTEGER NOT NULL REFERENCES companies(id) DEFERRABLE
        )
    '''
    run_sql_query(connection, query)

    # expected tables list

    # users
    query = '''
        CREATE TABLE users (
            id INTEGER PRIMARY KEY,
            name varchar(100),
            department_id INTEGER NOT NULL REFERENCES departments(id) DEFERRABLE
        )
    '''
    run_sql_query(connection, query)

    # desks
    query = '''
        CREATE TABLE desks (
            id INTEGER PRIMARY KEY,
            name varchar(100),
            user_id INTEGER NOT NULL REFERENCES users(id) DEFERRABLE,
            company_id INTEGER NOT NULL REFERENCES companies(id) DEFERRABLE
        )
    '''
    run_sql_query(connection, query)

    users_query = (
        'WITH ids AS (SELECT {table}.* FROM {table}) '
        'SELECT users.* FROM users '
        'INNER JOIN departments ON users.department_id=departments.id '
        'WHERE departments.company_id IN (SELECT id FROM ids)'
    )
    departments_query = (
        'WITH ids AS (SELECT {table}.* FROM {table}) '
        'SELECT departments.* FROM departments '
        'WHERE departments.company_id IN (SELECT id FROM ids)'
    )
    desks_query = (
        'WITH ids AS (SELECT {table}.* FROM {table}) '
        'SELECT desks.* FROM desks '
        'WHERE desks.company_id IN (SELECT id FROM ids)'
    )

    # bank_accounts is being referenced by companies,
    # so the query is more complicated
    bank_accounts_query = '''
            WITH orig_query AS (SELECT {table}.* FROM {table})

            SELECT bank_accounts.* FROM bank_accounts
            WHERE bank_accounts.id IN (
                SELECT companies.bank_account_id FROM companies
                INNER JOIN orig_query ON orig_query.id = companies.id
            )
        '''

    exp_tables = [
        Table(
            'departments',
            query=departments_query,
            post_import_actions=[],
            follow_relations=False,
        ),
        Table(
            'users', query=users_query, post_import_actions=[], follow_relations=False,
        ),
        Table(
            'desks', query=desks_query, post_import_actions=[], follow_relations=False,
        ),
        Table(
            'bank_accounts',
            query=bank_accounts_query,
            post_import_actions=[],
            follow_relations=False,
        ),
    ]

    # test

    tables = [Table('companies', follow_relations=True)]

    relations = follow_relations(connection, tables)

    assert sorted(relations) == sorted(exp_tables)
