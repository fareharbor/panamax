ARGS="--host=$POSTGRES_HOST --port=$POSTGRES_PORT --username=$POSTGRES_USER"

echo 'TRUNCATE TABLE users, companies CASCADE;' | psql ${ARGS} $POSTGRES_DB
