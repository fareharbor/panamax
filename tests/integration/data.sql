BEGIN;

CREATE TABLE companies (
  id INTEGER PRIMARY KEY NOT NULL,
  name varchar(100)
);

CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  name varchar(100),
  company_id INTEGER NOT NULL REFERENCES companies(id) DEFERRABLE
);

-- create two companies
INSERT INTO companies VALUES (1, 'company 1');
INSERT INTO companies VALUES (2, 'company 2');

-- and one company to test dynamic variables
INSERT INTO companies VALUES (3, 'dynamic-company');

-- create some users
INSERT INTO users VALUES (1, 'user 1', 1);
INSERT INTO users VALUES (2, 'user 2', 1);
INSERT INTO users VALUES (3, 'user 3', 2);

COMMIT;
