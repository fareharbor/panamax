PGARGS="--host=$POSTGRES_HOST --port=$POSTGRES_PORT --username=$POSTGRES_USER"

set -e

make_dump() {
    DUMP_FILENAME=/tmp/dump.sql.gz

    rm -f /tmp/dump.sql.gz

    cd /panamax/

    python -m panamax \
        -f tests/integration/test.toml \
        -u $POSTGRES_USER \
        -a $POSTGRES_HOST \
        -p $POSTGRES_PORT \
        -d $POSTGRES_DB \
        -o ${DUMP_FILENAME} \
        --gzip

    /panamax/tests/integration/cleardb.sh

    echo "#######################"
    echo "dump.sql.gz content:"
    zcat ${DUMP_FILENAME}

    echo "#######################"
    echo "Loading the dump..."
    zcat ${DUMP_FILENAME} | psql ${PGARGS} $POSTGRES_DB
}

assert_equal() {
    value=$(psql ${PGARGS} $POSTGRES_DB -c "$1" -t | head -n 1 )
    value="$(echo -e "${value}" | tr -d '[:space:]')"
    expected=$2

    if [ ${expected} != "${value}" ]
    then
        echo "NOT OK: ${value} != ${expected}"
        exit 1
    else
        echo "OK"
    fi
}

#####
make_dump

# we should have 2 users
echo "Check that there are two users in the database"
query="SELECT COUNT(*) FROM users;"
assert_equal "$query" "2"

# we should have 2 users
echo "Check that there are two users in the database for company_id=1"
query="SELECT COUNT(*) FROM users WHERE company_id = 1;"
assert_equal "$query" "2"

# both of them are with the same name: 'test'
# after the post_import_actions
echo "Users should have the same sanitized name 'test' (after applying post import actions)"
query="SELECT COUNT(*) FROM users WHERE name = 'test';"
assert_equal "$query" "2"

# check companies
echo "Check number of companies (should be 1)"
query="SELECT COUNT(*) FROM companies;"
assert_equal "$query" "2"  # 2 companies: one from dynamic and one from static vars

# check companies
echo "The company has id=1"
query="SELECT COUNT(*) FROM companies WHERE id = 1;"
assert_equal "$query" "1"

echo "The company has id=3"
query="SELECT COUNT(*) FROM companies WHERE name = 'dynamic-company';"
assert_equal "$query" "1"
