DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ARGS="--host=$POSTGRES_HOST --port=$POSTGRES_PORT --username=$POSTGRES_USER"

dropdb ${ARGS} $POSTGRES_DB
createdb ${ARGS} $POSTGRES_DB

cat ${DIR}/data.sql | psql ${ARGS} $POSTGRES_DB
