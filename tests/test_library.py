import pytest


def test_import():
    # check that the library's API is working
    try:
        from panamax import DynamicVariable, Variable, Table, dump  # noqa
    except ImportError as exc:
        pytest.fail('Faled to import: %s' % exc)
