from panamax.manifest.variables import (
    load_variables,
    DynamicVariable,
    Variable,
    _load_dynamic_variable,
    _update_variables,
)

from tests.conftest import run_sql_query


def test_load_variables_override(connection):
    variables = [Variable('user_id', 1)]
    overrides = {
        'user_id': 2,
        'company_id': 3,
    }

    variables = load_variables(connection, variables, overrides)
    exp_variables = [Variable(n, v) for n, v in overrides.items()]

    assert sorted(variables) == sorted(exp_variables)


def test_load_dynamic_variable(connection):
    # test that the function gets dynamic variable value
    # from the database and returns a Variable instance
    query = 'CREATE TABLE users (id INTEGER PRIMARY KEY)'
    run_sql_query(connection, query)

    user_ids = [1, 2]

    for _id in user_ids:
        run_sql_query(connection, 'INSERT INTO users (id) VALUES (%s)', params=(_id,))

    name = 'user_ids'
    query = 'SELECT id FROM users WHERE id IN (1, 2)'
    dynamic_variable = DynamicVariable(name, query)

    result_variable = _load_dynamic_variable(connection, dynamic_variable, [])

    # _load_dynamic_variable should get (1, 2) from the database
    assert result_variable == Variable(name, user_ids)


def test_load_dynamic_variable_with_variable_in_query(connection):
    # test that the function gets dynamic variable value
    # from the database and returns a Variable instance
    query = 'CREATE TABLE users (id INTEGER PRIMARY KEY)'
    run_sql_query(connection, query)

    user_id = 1

    run_sql_query(connection, 'INSERT INTO users (id) VALUES (%s)', params=(user_id,))

    static_variable = Variable('user_id', user_id)

    name = 'all_user_ids'
    query = 'SELECT id FROM users WHERE id={user_id}'
    dynamic_variable = DynamicVariable(name, query)

    result_variable = _load_dynamic_variable(
        connection, dynamic_variable, [static_variable]
    )

    # _load_dynamic_variable should get 1 from the database
    # and return a variable with a list as a value: [1]
    assert result_variable == Variable(name, [user_id])


def test_update_variables():
    # check that _update_variables returns a new list
    # of variables with updated values,
    # and doesn't change initial arguments
    not_in_overrides_variable = Variable('car_id', 100)
    variable = Variable('user_id', 1)
    overrides = {'user_id': 5, 'company_id': 7}

    exp_variables = [Variable(n, v) for n, v in overrides.items()]
    exp_variables.append(not_in_overrides_variable)

    all_variables = [not_in_overrides_variable, variable]

    overrides_copy = overrides.copy()
    all_variables_copy = all_variables[:]

    result_variables = _update_variables(all_variables, overrides)

    # check that all_variables and overrides are intact
    assert sorted(overrides_copy) == sorted(overrides)
    assert sorted(all_variables_copy) == sorted(all_variables)

    # check result_variables
    assert sorted(exp_variables) == sorted(result_variables)

def test_load_dynamic_variable_with_dynamic_variable_in_query(connection):
    query = 'CREATE TABLE users (id INTEGER PRIMARY KEY, location varchar(40))'
    run_sql_query(connection, query)

    user_location = 'some-location'

    run_sql_query(connection, 'INSERT INTO users (id, location) VALUES (%s, %s)', params=(1, user_location))
    run_sql_query(connection, 'INSERT INTO users (id, location) VALUES (%s, %s)', params=(2, user_location))

    base_query = 'SELECT DISTINCT location FROM users'
    base_variable = DynamicVariable('location_names', base_query)

    name = 'all_user_ids'
    secondary_query = 'SELECT id FROM users WHERE location=ANY({location_names})'
    secondary_variable = DynamicVariable(name, secondary_query)

    variables = load_variables(connection, [base_variable, secondary_variable], {})

    # the base_variable should return ['some-location']
    # and the secondary_variable should use the resut from base_variable
    # in its WHERE clause and return a list of user ids [1, 2]
    assert variables[0].name == 'location_names'
    assert variables[0].value == ['some-location']

    assert variables[1].name == 'all_user_ids'
    assert variables[1].value == [1, 2]
