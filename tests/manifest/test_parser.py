import pytest

from panamax.manifest import (
    DynamicVariable,
    Variable,
    Table,
)
from panamax.manifest.parser import (
    loads,
    load,
    InvalidManifestException,
)


EXP_SIMPLE_MANIFEST = (
    [],
    [Table(name='users', query='SELECT users.* FROM users')],
)


def test_simple_manifest_load():
    variables, tables = load("tests/data/simple.toml")

    _assert_manifests_equal([variables, tables], EXP_SIMPLE_MANIFEST)


def test_simple_manifest_loads_from_string():
    data = open("tests/data/simple.toml").read()
    variables, tables = loads(data)

    _assert_manifests_equal([variables, tables], EXP_SIMPLE_MANIFEST)


EXP_MANIFEST = (
    # test doesn't support dynamic variables yet
    [
        DynamicVariable(
            name='group_ids', query='SELECT id FROM groups WHERE name ILIKE \'%admin%\''
        ),
        Variable(name='user_id', value=123),
        Variable(name='companies', value=[1, 2, 3]),
    ],
    [
        Table(
            name='users',
            query=(
                "SELECT users.* FROM users "
                "WHERE users.id = {user_id} OR users.group_id = ANY({group_ids})"
            ),
        ),
        Table(
            name='groups',
            query="SELECT * FROM groups",
            post_import_actions=["UPDATE users SET name = 'sanitized'"],
        ),
    ],
)


def test_complex_manifest_load():
    variables, tables = load("tests/data/manifest.toml")

    _assert_manifests_equal([variables, tables], EXP_MANIFEST)


def test_complex_manifest_loads_from_string():
    data = open("tests/data/manifest.toml").read()
    variables, tables = loads(data)

    _assert_manifests_equal([variables, tables], EXP_MANIFEST)


def _assert_manifests_equal(manifest, exp_manifest):
    variables = sorted(manifest[0])
    exp_variables = sorted(exp_manifest[0])

    for i in range(len(variables)):
        assert variables[i] == exp_variables[i]

    tables = _tables_to_dicts(manifest[1])
    exp_tables = _tables_to_dicts(exp_manifest[1])

    for i in range(len(tables)):
        assert tables[i] == exp_tables[i]


def _tables_to_dicts(tables):
    return sorted(
        [
            {
                'name': t.name,
                'query': t.query,
                'post_import_actions': t.post_import_actions,
            }
            for t in tables
        ],
        key=lambda t: t['name'],
    )


def test_invalid_manifest():
    manifest = '''
    [[tab]]
    '''

    with pytest.raises(InvalidManifestException):
        loads(manifest)


def test_invalid_table_in_manifest():
    manifest = '''
    [[table]]
    key = 1
    '''

    with pytest.raises(InvalidManifestException):
        loads(manifest)
