import mock

from psycopg2 import sql
import psycopg2.extensions

from panamax import db


def test_connect():
    database = "mydb"
    host = "127.0.0.1"
    port = 5432
    user = "root"
    password = "secret"

    exp_connection = 'connection'
    mocked_connect = mock.Mock(return_value=exp_connection)

    with mock.patch('panamax.db.psycopg2.connect', mocked_connect):
        connection = db.connect(database, host, port, user, password)

    mocked_connect.assert_called_once_with(
        database=database, user=user, password=password, host=host, port=port,
    )

    assert connection == exp_connection


def test_get_table_columns():
    table = 'some_table'
    exp_query = '''
        SELECT column_name
        FROM information_schema.columns
        WHERE table_schema = 'public'
              AND table_name=%s
    '''

    exp_columns = [('id',), ('username',)]

    mocked_connection = mock.Mock()

    mocked_cursor = mock.Mock()
    mocked_connection.cursor.return_value = mocked_cursor
    mocked_cursor.fetchall.return_value = exp_columns

    # check the result
    assert db.get_table_columns(mocked_connection, table) == [c[0] for c in exp_columns]

    mocked_cursor.execute.assert_called_once_with(exp_query, (table,))


def test_copy_to():
    mocked_out = mock.Mock()
    exp_query = 'SELECT 1'

    mocked_connection = mock.Mock()
    mocked_cursor = mock.Mock()
    mocked_connection.cursor.return_value = mocked_cursor

    db.copy_to(mocked_connection, mocked_out, exp_query)

    mocked_cursor.copy_to.assert_called_once_with(mocked_out, exp_query)


def test_execute():
    exp_query = 'SELECT 1'

    mocked_connection = mock.Mock()
    mocked_cursor = mock.Mock()
    mocked_connection.cursor.return_value = mocked_cursor

    exp_result = [['a'], ['b']]
    mocked_cursor.fetchall.return_value = exp_result

    result = db.execute(mocked_connection, exp_query)

    mocked_cursor.execute.assert_called_once_with(exp_query, [])

    assert result == exp_result


def test_format_sql():
    # check that format_sql uses psycopg2.sql module
    # and returns a string ready to be executed
    mocked_connection = mock.Mock(spec=psycopg2.extensions.connection)
    query = "SELECT {table}.* FROM {table} WHERE id={user_id}"
    user_id = 5
    literals = {'user_id': user_id}
    table_name = 'users'
    identifiers = {'table': table_name}

    def mocked_as_string(self, connection):
        """
        Mocked psycopg2.sql.Composed.as_string to return the object itself.

        We don't have a postgresql instance in tests,
        so we can't call as_string with a real connection.
        """
        return self

    with mock.patch.object(sql.Composed, "as_string", mocked_as_string):
        formatted_query = db.format_sql(mocked_connection, query, identifiers, literals)

    exp_composed = sql.Composed(
        [
            sql.SQL('SELECT '),
            sql.Identifier(table_name),
            sql.SQL('.* FROM '),
            sql.Identifier(table_name),
            sql.SQL(' WHERE id='),
            sql.Literal(user_id),
        ]
    )

    assert formatted_query == exp_composed
