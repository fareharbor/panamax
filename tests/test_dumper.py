import mock
import pytest

from panamax.dumper import (
    POST_IMPORT_ACTIONS,
    _dump_table,
    dump_manifest_files,
    _format_beginning_table_dump,
    dump,
)
from panamax import manifest

from tests.conftest import patched_format_sql, run_sql_query

EXP_DEFAULT_QUERY = "SELECT {table}.* FROM {table}"

EXP_BEGIN_DUMP = """
--
-- PostgreSQL database dump
--

BEGIN;

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET CONSTRAINTS ALL DEFERRED;

SET search_path = public, pg_catalog;
"""

EXP_BEGIN_TABLE_DUMP = """
--
-- Data for table: {table}
--
CREATE TEMP TABLE IF NOT EXISTS tmp_{table}
  ON COMMIT DROP
  AS
  SELECT *
  FROM {table}
  WITH NO DATA;


COPY tmp_{table} ({columns}) FROM stdin;
"""

EXP_DATA_SEPARATOR = '\\.'

EXP_END_TABLE_DUMP = r"""

INSERT INTO {table}
SELECT *
FROM tmp_{table}
ON CONFLICT DO NOTHING;

TRUNCATE TABLE tmp_{table};
"""

EXP_POST_IMPORT_ACTIONS = """\n
-- Post actions for table {table}
{queries}
"""

EXP_END_DUMP = """
COMMIT;
"""


@pytest.fixture()
def patch_format_sql():
    with mock.patch('panamax.manifest.tables.db.format_sql', patched_format_sql):
        with mock.patch('panamax.dumper.db.format_sql', patched_format_sql):
            yield


def _format_post_import_actions(name, post_import_actions, variables=None):
    if variables is None:
        variables = []

    variables_dict = {v.name: v.value for v in variables}

    if post_import_actions:
        queries_str = '\n'.join(map(lambda x: '%s;' % x, post_import_actions)).format(
            **variables_dict
        )
        return POST_IMPORT_ACTIONS.format(table=name, queries=queries_str)
    else:
        return ''


@pytest.mark.parametrize(
    'table_name, table_columns',
    [('users', ['id', 'name', 'username']), ('users', ['id'])],
)
def test_format_beginning_table_dump(table_name, table_columns):
    # check that `_format_beginning_table_dump` returns a string
    # with correct data for a dump
    mocked_connection = mock.Mock()

    table = manifest.Table(table_name)

    columns_str = ', '.join(map(lambda x: '"%s"' % x, table_columns))
    exp_dump_str = EXP_BEGIN_TABLE_DUMP.format(table=table.name, columns=columns_str)

    with mock.patch('panamax.dumper.db.get_table_columns', return_value=table_columns):
        dump_str = _format_beginning_table_dump(mocked_connection, table)

    assert dump_str == exp_dump_str


def test_dump_table(patch_format_sql):
    # check `_dump_table` function:
    # it should dump a table from a database
    out = mock.Mock()
    post_import_actions = [
        "UPDATE companies SET name = NULL",
        "DELETE FROM companies WHERE id = 5",
    ]
    query = "SELECT * FROM companies WHERE id = {company_id}"
    table = manifest.Table(
        name="companies", query=query, post_import_actions=post_import_actions
    )

    variables = [manifest.Variable("company_id", 1)]

    mocked_db_copy_to = mock.Mock()

    columns = ["id", "name"]
    mocked_db_get_table_columns = mock.Mock(return_value=columns)
    mocked_connection = mock.Mock()

    with mock.patch("panamax.dumper.db.copy_to", mocked_db_copy_to):
        with mock.patch(
            "panamax.dumper.db.get_table_columns", mocked_db_get_table_columns
        ):
            _dump_table(mocked_connection, out, table, variables)

    # test db.get_table_columns call
    mocked_db_get_table_columns.call_args == mock.call(mocked_connection, table)

    # test written output
    _assert_table_dump(out, table.name, columns, variables, post_import_actions, query)

    # check db.copy_to call
    exp_query = "(%s)" % query.format(
        table=table.name, **{v.name: v.value for v in variables}
    )
    assert mocked_db_copy_to.call_args == mock.call(mocked_connection, out, exp_query)


def _assert_table_dump(out, table, columns, variables, post_import_actions, query):
    """
    Checks that table dump has been written to `out`.

    Does not check db.copy_to function,
    checks that beginning and ending of the table dump
    has been written to the `out` writer.
    """
    exp_call_count = 4  # start, data separator, post_import_actions, end
    # test written output
    assert out.write.call_count == exp_call_count

    # exp beginning of the dump
    exp_columns = ", ".join(map(lambda x: '"%s"' % x, columns))
    assert out.write.mock_calls[0] == mock.call(
        EXP_BEGIN_TABLE_DUMP.format(table=table, columns=exp_columns)
    )

    assert out.write.mock_calls[1] == mock.call(EXP_DATA_SEPARATOR)

    # post actions
    if post_import_actions:
        exp_post_import_actions = _format_post_import_actions(table, post_import_actions)
    else:
        exp_post_import_actions = ''

    assert out.write.mock_calls[2] == mock.call(exp_post_import_actions)

    exp_end_table_dump = EXP_END_TABLE_DUMP.format(table=table)
    assert out.write.mock_calls[3] == mock.call(exp_end_table_dump)


def test_dump_table_default_query_without_post_import_actions(patch_format_sql):
    # check _dump_table with default query and without any post import actions
    out = mock.Mock()
    variables = [manifest.Variable("company_id", 1)]

    table = manifest.Table(name="companies")

    mocked_connection = mock.Mock()
    mocked_db_copy_to = mock.Mock()

    columns = ["id", "name", "fields"]
    mocked_db_get_table_columns = mock.Mock(return_value=columns)

    with mock.patch("panamax.dumper.db.copy_to", mocked_db_copy_to):
        with mock.patch(
            "panamax.dumper.db.get_table_columns", mocked_db_get_table_columns
        ):
            _dump_table(mocked_connection, out, table, variables)

    # test db.get_table_columns call
    mocked_db_get_table_columns.call_args == mock.call(mocked_connection, table)

    # test written output
    _assert_table_dump(
        out,
        table.name,
        columns,
        variables,
        post_import_actions=[],
        query=EXP_DEFAULT_QUERY,
    )

    # check db.copy_to call
    exp_query = "(%s)" % EXP_DEFAULT_QUERY.format(
        table=table.name, **{v.name: v.value for v in variables}
    )
    assert mocked_db_copy_to.call_args == mock.call(mocked_connection, out, exp_query)


_TEST_MANIFEST = [
    [
        manifest.Variable(name="user_id", value=1),
        manifest.Variable(name="group_ids", value=[1, 2, 3]),
    ],
    [
        manifest.Table(
            name="users",
            query=(
                "SELECT users.* FROM users "
                "WHERE users.id = {user_id} OR users.group_id = ANY({group_ids})"
            ),
        ),
        manifest.Table(
            name="groups",
            query="SELECT * FROM groups",
            post_import_actions=["UPDATE users SET name = 'sanitized' AND id={user_id}"],
        ),
    ],
]


@mock.patch("panamax.dumper.db.copy_to")
@mock.patch("panamax.dumper.manifest.load", return_value=_TEST_MANIFEST)
def test_dump_manifest_files(mocked_manifest_load, mocked_copy_to, patch_format_sql):
    manifest_filename = "file.toml"
    output_filename = "out.sql"

    columns = {
        "users": ["id", "name"],
        "groups": ["id", "name", "permissions"],
    }

    # mock get_table_columns
    def _mocked_get_columns(_, table):
        return columns[table]

    import psycopg2.extensions

    mocked_connection = mock.Mock(spec=psycopg2.extensions.connection)

    # mock output
    mocked_out = mock.Mock()
    mocked_open = mock.Mock(return_value=mocked_out)

    with mock.patch("panamax.dumper.open", mocked_open, create=True):
        with mock.patch("panamax.dumper.db.get_table_columns", _mocked_get_columns):
            dump_manifest_files(
                mocked_connection, [manifest_filename], output_filename,
            )

    mocked_open.assert_called_once_with(output_filename, "wb")

    mocked_manifest_load.assert_called_once_with(manifest_filename)

    # check that the data is correct
    assert (
        mocked_out.write.call_count == 10
    )  # begin + end + 2*tables + 2* data separators + 2*tables(post_import_actions)

    # copy_to calls
    assert mocked_copy_to.call_count == 2  # 2 tables

    # beginning of the dump
    assert mocked_out.write.mock_calls[0] == mock.call(EXP_BEGIN_DUMP)

    # first table
    table = _TEST_MANIFEST[1][0]
    exp_columns = ", ".join(map(lambda x: '"%s"' % x, columns[table.name]))
    exp_table_begin = EXP_BEGIN_TABLE_DUMP.format(table=table.name, columns=exp_columns)
    assert mocked_out.write.mock_calls[1] == mock.call(exp_table_begin)

    # end of the first table
    assert mocked_out.write.mock_calls[2] == mock.call(EXP_DATA_SEPARATOR)
    assert mocked_out.write.mock_calls[3] == mock.call('')  # empty post import actions
    assert mocked_out.write.mock_calls[4] == mock.call(
        EXP_END_TABLE_DUMP.format(table=table.name)
    )

    # second table
    table = _TEST_MANIFEST[1][1]
    exp_columns = ", ".join(map(lambda x: '"%s"' % x, columns[table.name]))
    exp_table_begin = EXP_BEGIN_TABLE_DUMP.format(table=table.name, columns=exp_columns)
    assert mocked_out.write.mock_calls[5] == mock.call(exp_table_begin)

    assert mocked_out.write.mock_calls[6] == mock.call(EXP_DATA_SEPARATOR)

    # post import actions
    post_import_actions = _TEST_MANIFEST[1][1].post_import_actions
    exp_end_table_dump = _format_post_import_actions(
        table.name, post_import_actions, variables=_TEST_MANIFEST[0]
    )
    assert mocked_out.write.mock_calls[7] == mock.call(exp_end_table_dump)

    # end of the second table
    assert mocked_out.write.mock_calls[8] == mock.call(
        EXP_END_TABLE_DUMP.format(table=table.name)
    )

    # end of the dump
    assert mocked_out.write.mock_calls[9] == mock.call(EXP_END_DUMP)


def test_dump_with_extra_variables(connection):
    # create companies table
    query = 'CREATE TABLE companies (id INTEGER PRIMARY KEY, name varchar(100))'
    run_sql_query(connection, query)

    company_name = 'mycompany'
    # insert a company
    run_sql_query(
        connection,
        'INSERT INTO companies (id, name) VALUES (%s, %s)',
        params=(1, company_name),
    )

    companies_query = 'SELECT {table}.* FROM {table} WHERE name={company_name}'
    tables = [manifest.Table('companies', query=companies_query)]

    extra_variables = {'company_name': company_name}

    # mock output
    mocked_out = mock.Mock()
    mocked_open = mock.Mock(return_value=mocked_out)

    with mock.patch("panamax.dumper.open", mocked_open, create=True):
        dump(
            connection,
            manifests=[[[], tables]],
            output_filename='',
            extra_variables=extra_variables,
        )

    # check that the data is correct
    assert (
        mocked_out.write.call_count == 7
    )  # begin + end + begin_table + table + empty + data separator + post_import_actions + end_table

    exp_calls = [
        mock.call.write(EXP_BEGIN_DUMP),
        mock.call.write(
            EXP_BEGIN_TABLE_DUMP.format(table='companies', columns='"id", "name"')
        ),
        mock.call.write(b'1\tmycompany\n'),
        mock.call.write('\\.'),
        mock.call.write(''),
        mock.call.write(
            '\n\nINSERT INTO companies\nSELECT *\nFROM tmp_companies\nON '
            'CONFLICT DO NOTHING;\n\nTRUNCATE TABLE tmp_companies;\n'
        ),
        mock.call.flush(),
        mock.call.write('\nCOMMIT;\n'),
        mock.call.close(),
    ]

    assert mocked_out.mock_calls == exp_calls
