import os

import pytest

import psycopg2


def patched_format_sql(connection, query, identifiers=None, literals=None):
    """
    Simplified version of panamax.utils.format_sql
    to avoid using psycopg2.extensions.connection

    Usage:


    ```python
    from tests.conftest import patched_format_sql

    @pytest.fixture(scope='module', autouse=True)
    def patch_format_sql():
        with mock.patch('panamax.manifest.tables.utils.format_sql', patched_format_sql):
            yield
    ```
    """
    # mocked version of utils.format_sql,
    # just formats a string using python formatter
    if identifiers is None:
        identifiers = {}

    if literals is None:
        literals = {}

    params = identifiers.copy()
    params.update(literals.copy())

    return query.format(**params)


@pytest.fixture()
def connection(scope='function'):
    connection = psycopg2.connect(
        database=os.environ['POSTGRES_DB'],
        user=os.environ['POSTGRES_USER'],
        password=os.environ['POSTGRES_PASSWORD'],
        host=os.environ['POSTGRES_HOST'],
        port=os.environ['POSTGRES_PORT'],
    )

    yield connection

    connection.close()


@pytest.fixture(autouse=True)
def _clear_database(connection):
    _drop_db_schema(connection)
    yield
    _drop_db_schema(connection)


def _drop_db_schema(connection):
    c = connection.cursor()
    c.execute('DROP SCHEMA public CASCADE')
    c.close()

    c = connection.cursor()
    c.execute('CREATE SCHEMA public')
    c.close()


def run_sql_query(connection, query, params=None):
    cursor = connection.cursor()
    cursor.execute(query, params or tuple())
    cursor.close()
