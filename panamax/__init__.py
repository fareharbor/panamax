__version__ = '0.6.0'


VERSION = __version__

from .manifest import Variable, DynamicVariable, Table  # noqa
from .dumper import dump, dump_manifest_files  # noqa
