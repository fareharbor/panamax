import gzip
import logging
from typing import List, Optional, BinaryIO, Union, Dict, Any, cast, Tuple

import psycopg2
from psycopg2 import sql
import psycopg2.extras


logger = logging.getLogger(__name__)


def connect(database, host, port, user, password):
    # type: (str, str, int, str, str) -> psycopg2.extensions.connection
    return psycopg2.connect(
        database=database, user=user, password=password, host=host, port=port,
    )


def get_tables(connection):
    # type: (psycopg2.extensions.connection) -> List[Dict[str, str]]
    query = (
        "SELECT table_name FROM information_schema.tables "
        "WHERE table_schema = 'public'"
    )
    tables = execute(connection, query, dictionary_cursor=True)
    tables = cast(List[Dict[str, str]], tables)

    return tables


def get_table_columns(connection, table):
    # type: (psycopg2.extensions.connection, str) -> List[str]
    query = """
        SELECT column_name
        FROM information_schema.columns
        WHERE table_schema = 'public'
              AND table_name=%s
    """

    cursor = connection.cursor()
    cursor.execute(query, (table,))
    columns = cursor.fetchall()

    return [c[0] for c in columns]


def copy_to(connection, out, query):
    # type: (psycopg2.extensions.connection, Union[BinaryIO, gzip.GzipFile], str) -> None
    cursor = connection.cursor()

    try:
        cursor.copy_to(out, query)
    except psycopg2.Error:
        logger.error(
            'Error occured during executing COPY TO stdout with query="%s"',
            query,
            exc_info=True,
        )
        raise

    cursor.close()


def execute(
    connection,  # type: psycopg2.extensions.connection
    query,  # type: str
    params=None,  # type: Union[List[Any], Tuple[Any, ...]]
    dictionary_cursor=False,  # type: Optional[bool]
):  # type: (...) -> Union[List[List[Any]], List[Dict[str, Any]]]
    if params is None:
        params = []

    cursor_kwargs = {}  # type: Dict[str, Any]
    if dictionary_cursor:
        cursor_kwargs = {'cursor_factory': psycopg2.extras.RealDictCursor}

    cursor = connection.cursor(**cursor_kwargs)

    try:
        cursor.execute(query, params)
    except psycopg2.Error:
        logger.error('Error occured during executing query=%s', query, exc_info=True)
        raise

    data = cursor.fetchall()

    cursor.close()

    return data


def format_sql(connection, query, identifiers=None, literals=None):
    # type: (psycopg2.extensions.connection, str, Optional[Dict[str, Any]], Optional[Dict[str, Any]]) -> str
    """
    Formats `query` with variables and returns an SQL query ready to be executed.
    """
    if identifiers is None:
        identifiers = {}

    if literals is None:
        literals = {}

    params = {n: sql.Identifier(i) for n, i in identifiers.items()}
    params.update({n: sql.Literal(l) for n, l in literals.items()})

    sql_query = sql.SQL(query).format(**params)

    return sql_query.as_string(connection)
