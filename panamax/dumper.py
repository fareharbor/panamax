import gzip
import logging
from typing import List, BinaryIO, Union, TYPE_CHECKING, Optional, Dict

import psycopg2

from panamax import manifest, db
from panamax.manifest.variables import load_variables
from panamax.manifest.tables import format_table_query
from panamax.analyze import relations


logger = logging.getLogger(__name__)


if TYPE_CHECKING:
    from panamax.manifest import Manifest


DEFAULT_QUERY = 'SELECT {table}.* FROM {table}'

BEGIN_DUMP = '''
--
-- PostgreSQL database dump
--

BEGIN;

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET CONSTRAINTS ALL DEFERRED;

SET search_path = public, pg_catalog;
'''

BEGIN_TABLE_DUMP = '''
--
-- Data for table: {table}
--
CREATE TEMP TABLE IF NOT EXISTS tmp_{table}
  ON COMMIT DROP
  AS
  SELECT *
  FROM {table}
  WITH NO DATA;


COPY tmp_{table} ({columns}) FROM stdin;
'''

DATA_SEPARATOR = '\\.'

END_TABLE_DUMP = r'''

INSERT INTO {table}
SELECT *
FROM tmp_{table}
ON CONFLICT DO NOTHING;

TRUNCATE TABLE tmp_{table};
'''

POST_IMPORT_ACTIONS = '''\n
-- Post actions for table {table}
{queries}
'''

END_DUMP = '''
COMMIT;
'''


def dump_manifest_files(
    connection,  # type: psycopg2.extensions.connection
    manifest_files,  # type: List[str]
    output_filename,  # type: str
    gzip_compress=False,  # type: bool
    compare_db_tables=False,  # type: bool
    extra_variables=None,  # type: Optional[Dict[str, str]]
):
    '''
    Dumps given list of `manifests` as an SQL dump to a file.
    '''
    manifests = [manifest.load(f) for f in manifest_files]
    dump(
        connection,
        manifests,
        output_filename,
        gzip_compress,
        extra_variables=extra_variables,
    )


def dump(
    connection,  # type: psycopg2.extensions.connection
    manifests,  # type: List[Manifest]
    output_filename,  # type: str
    gzip_compress=False,  # type: bool
    compare_db_tables=False,  # type: bool
    extra_variables=None,  # type: Optional[Dict[str, str]]
):  # type: (...) -> None
    '''
    Dumps given list of `manifests` as an SQL dump to a file.
    '''
    if gzip_compress:
        out = gzip.open(output_filename, 'wb')  # type: Union[gzip.GzipFile, BinaryIO]
    else:
        out = open(output_filename, 'wb')
    out.write(BEGIN_DUMP)

    for variables, tables in manifests:
        _dump_manifest(
            connection,
            out,
            variables,
            tables,
            compare_db_tables,
            extra_variables=extra_variables,
        )

    out.write(END_DUMP)
    out.close()

    logger.info('Successfully completed. Output filename=%s', output_filename)


def _dump_manifest(
    connection,  # type: psycopg2.extensions.connection
    out,  # type: Union[BinaryIO, gzip.GzipFile]
    variables,  # type: List[Union[manifest.Variable, manifest.DynamicVariable]]
    tables,  # type: List[manifest.Table]
    compare_db_tables=False,  # type: bool
    extra_variables=None,  # type: Optional[Dict[str, str]]
):  # type: (...) -> None
    '''
    Dumps `manifest` to the `out` writer.
    '''
    if extra_variables is None:
        extra_variables = {}

    logger.info('Dumping a manifest')

    static_variables = load_variables(connection, variables, overrides=extra_variables)

    tables += relations.follow_relations(connection, tables)

    if compare_db_tables:
        relations.compare_tables_with_database(connection, tables)

    total = len(tables)

    for i, table in enumerate(tables, 1):
        _dump_table(connection, out, table, static_variables)
        logger.info('[%s/%s] dumped table=%s', i, total, table.name)


def _dump_table(
    connection,  # type: psycopg2.extensions.connection
    out,  # type: Union[BinaryIO, gzip.GzipFile]
    table,  # type: manifest.Table
    variables,  # type: List[manifest.Variable]
):  # type: (...) -> None
    '''
    Dumps `table` to the `out` writer.
    '''
    query = format_table_query(connection, table, variables)
    logger.info('dumping table=%s query=%s', table.name, query)

    data = _format_beginning_table_dump(connection, table)
    out.write(data)

    db.copy_to(connection, out, '({})'.format(query))
    out.write(DATA_SEPARATOR)

    post_import_actions = _format_post_import_actions(connection, variables, table)
    out.write(post_import_actions)

    data = END_TABLE_DUMP.format(table=table.name)
    out.write(data)

    out.flush()


def _format_beginning_table_dump(connection, table):
    # type: (Union[BinaryIO, gzip.GzipFile], manifest.Table) -> str
    table_columns = db.get_table_columns(connection, table.name)
    table_columns_str = ', '.join(map(lambda x: '"%s"' % x, table_columns))

    return BEGIN_TABLE_DUMP.format(table=table.name, columns=table_columns_str)


def _format_post_import_actions(connection, variables, table):
    # type: (psycopg2.extensions.connection, List[manifest.Variable], manifest.Table) -> str
    if not table.post_import_actions:
        return ''

    literals = {v.name: v.value for v in variables}
    identifiers = {'table': 'tmp_{}'.format(table.name)}

    queries = []

    for query in table.post_import_actions:
        queries.append(
            db.format_sql(connection, query, identifiers=identifiers, literals=literals)
        )

    queries_str = '\n'.join(map(lambda x: '%s;' % x, queries))

    return POST_IMPORT_ACTIONS.format(table=table.name, queries=queries_str)
