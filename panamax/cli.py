import time
import datetime
import argparse

import panamax.dumper
import panamax.utils


def _parse_extra_variables(extra_vars_string):
    # type (str) -> Dict[str, str]
    if not extra_vars_string:
        return {}

    return dict(e.split('=') for e in extra_vars_string.split(' '))


def main():
    parser = argparse.ArgumentParser(description='cool tool')
    parser.add_argument(
        '-f', '--file', nargs='+', required=True, help='Manifest filename'
    )
    parser.add_argument('-o', '--output', required=True, help='Output filename')
    parser.add_argument('-a', '--host', default='localhost', help='database host')
    parser.add_argument('-p', '--port', default=5432, help='database port')
    parser.add_argument('-u', '--user', help='database username')
    parser.add_argument('-d', '--database', required=True, help='database name')
    parser.add_argument('-l', '--loglevel', default='info', help='logging level')
    parser.add_argument(
        '-g',
        '--gzip',
        default=False,
        dest='gzip_output',
        action='store_true',
        help='compress the dump (gzip)',
    )
    parser.add_argument(
        '-c',
        '--compare-db-tables',
        default=True,
        dest='compare_db_tables',
        action='store_true',
        help='Compare DB tables and the manifest',
    )
    parser.add_argument(
        '-e',
        '--extra-vars',
        default=None,
        help='Extra variables: --extra-vars="k=v k2=v2"',
    )

    args = parser.parse_args()

    panamax.utils.setup_logging(level_name=args.loglevel)

    started_at = time.time()
    extra_variables = _parse_extra_variables(args.extra_vars)

    print('Extra variables: {}'.format(extra_variables))
    print('Dumping files: {}'.format(args.file))

    password = panamax.utils.get_password()

    connection = panamax.db.connect(
        args.database, args.host, args.port, args.user, password,
    )

    panamax.dump_manifest_files(
        connection=connection,
        manifest_files=args.file,
        output_filename=args.output,
        gzip_compress=args.gzip_output,
        compare_db_tables=args.compare_db_tables,
        extra_variables=extra_variables,
    )

    connection.close()

    total_time = datetime.timedelta(seconds=int(time.time() - started_at))
    msg = '---\n{}Successfully completed{} ({})\nDump file name: {}'.format(
        panamax.utils.tcolors.GREEN, panamax.utils.tcolors.ENDC, total_time, args.output,
    )
    print(msg)


if __name__ == '__main__':
    main()
