import os
import os.path
import sys
import logging
from getpass import getpass


logger = logging.getLogger(__name__)


class tcolors:
    GRAY = '\033[37m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    MAGENTA = '\033[95m'
    CYAN = '\033[96m'
    ENDC = '\033[0m'


class ColoredFormatter(logging.Formatter):
    _level_colors = {
        'WARNING': tcolors.YELLOW,
        'INFO': tcolors.BLUE,
        'DEBUG': tcolors.GRAY,
        'CRITICAL': tcolors.MAGENTA,
        'ERROR': tcolors.RED,
    }
    _msg_colors = {
        'DEBUG': tcolors.GRAY,
    }

    def format(self, record):
        msg_color = self._msg_colors.get(record.levelname, tcolors.ENDC)
        record.msg = '%s%s%s' % (msg_color, record.msg, tcolors.ENDC)

        level_color = self._level_colors.get(record.levelname, tcolors.ENDC)
        record.levelname = '[%s%s%s]' % (level_color, record.levelname, tcolors.ENDC)

        return logging.Formatter.format(self, record)


def setup_logging(level_name):
    # type: (str) -> None
    level = logging.getLevelName(level_name.upper())
    root = logging.getLogger()

    root.setLevel(level)

    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(level)

    logging_format = '%(levelname)-18s %(asctime)s %(thread)d %(name)-35s  %(message)s'
    time_format = '%Y-%m-%d %H:%M:%S'
    formatter = ColoredFormatter(logging_format, time_format)
    handler.setFormatter(formatter)
    root.addHandler(handler)


def get_password():
    # type: () -> str
    password = os.getenv('PGPASSWORD')
    if not password:
        password = getpass('Enter database password:')

    return password
