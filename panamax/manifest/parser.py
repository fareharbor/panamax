import logging
from typing import Any, List, MutableMapping, Union, TYPE_CHECKING

import toml

from panamax.manifest.tables import Table
from panamax.manifest.variables import DynamicVariable, Variable


if TYPE_CHECKING:
    from panamax.manifest import Manifest


logger = logging.getLogger(__name__)


class InvalidManifestException(Exception):
    pass


def loads(toml_string):
    # type: (str) -> Manifest
    logger.debug('Parsing string manifest')
    manifest = toml.loads(toml_string)

    return _parse(manifest)


def load(filename):
    # type: (str) -> Manifest
    logger.debug('Parsing manifest file: %s', filename)
    manifest = toml.load(filename)

    return _parse(manifest)


def _parse(manifest):
    # type: (MutableMapping[str, Any]) -> Manifest
    _validate(manifest)

    variables = [
        Variable(name, value) for name, value in manifest.get('variables', {}).items()
    ]  # type: List[Union[Variable, DynamicVariable]]

    variables += [
        DynamicVariable(name, query)
        for name, query in manifest.get('dynamic_variables', {}).items()
    ]

    tables = [
        Table(
            name=t['table'],
            query=t.get('query'),
            post_import_actions=t.get('post_import_actions', []),
            follow_relations=t.get('follow_relations', False),
        )
        for t in manifest['tables']
    ]  # type: List[Table]

    return variables, tables


def _validate(manifest):
    _validate_manifest_top_level(manifest)
    _validate_tables(manifest.get('tables', []))


def _validate_manifest_top_level(manifest):
    allowed_manifest_keys = {'dynamic_variables', 'variables', 'tables'}

    msg = 'Manifest contains unknown keys'
    _assert_allowed_keys(manifest, allowed_manifest_keys, msg)


def _assert_allowed_keys(dictionary, allowed_keys, msg):
    unknown_keys = set(dictionary.keys()) - allowed_keys

    if unknown_keys:
        logger.error('%s. keys: %s', msg, unknown_keys)

        raise InvalidManifestException()


def _validate_tables(tables):
    allowed_table_keys = {
        'table',
        'query',
        'post_import_actions',
        'follow_relations',
    }
    for t in tables:
        msg = 'Table "%s" contains unknown keys' % t['table']
        _assert_allowed_keys(t, allowed_table_keys, msg)
