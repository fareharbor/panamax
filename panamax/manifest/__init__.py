from typing import List, Union, Tuple

from panamax.manifest.variables import DynamicVariable, Variable  # noqa
from panamax.manifest.tables import Table  # noqa
from panamax.manifest.parser import loads, load  # noqa


Manifest = Tuple[List[Union[Variable, DynamicVariable]], List[Table]]
