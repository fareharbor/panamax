import logging
from collections import namedtuple
from typing import List, TYPE_CHECKING

from panamax.manifest.variables import Variable
from panamax import db

logger = logging.getLogger(__name__)


if TYPE_CHECKING:
    import psycopg2.extensions


DEFAULT_QUERY = 'SELECT {table}.* FROM {table}'


Table = namedtuple('Table', ['name', 'query', 'post_import_actions', 'follow_relations'])
Table.__new__.__defaults__ = (None, DEFAULT_QUERY, [], False)  # type: ignore


def format_table_query(connection, table, variables):
    # type: (psycopg2.extensions.connection, Table, List[Variable]) -> str
    """
    Formats query for a given `table`.
    The query can be used to dump the table.
    """
    logger.debug('formatting query=%s', table.query)

    literals = {v.name: v.value for v in variables}
    identifiers = {'table': table.name}

    return db.format_sql(
        connection, table.query, identifiers=identifiers, literals=literals
    )
