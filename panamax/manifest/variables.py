import logging
from collections import namedtuple
from typing import List, TYPE_CHECKING, Union, cast, Any, Dict

from panamax import db

if TYPE_CHECKING:
    import psycopg2.extensions


logger = logging.getLogger(__name__)


Variable = namedtuple('Variable', ['name', 'value'])
DynamicVariable = namedtuple('Variable', ['name', 'query'])


def load_variables(
    connection,  # type: psycopg2.extensions.connection
    variables,  # type: List[Union[DynamicVariable, Variable]]
    overrides,  # type: Dict[str, str]
):
    # type: (...) -> List[Variable]
    """
    Returns a list of variables ready to be used in queries:
        - overrides values with values from passed `overrides` list.
        - loads values of dynamic variables from a database.
    """
    static_variables = [v for v in variables if isinstance(v, Variable)]
    dynamic_variables = [v for v in variables if isinstance(v, DynamicVariable)]

    result_variables = _update_variables(static_variables, overrides)

    # Now we can build dynamic variables.
    # Dynamic variable can contain variables in its query,
    # so we have to pass the result_variables list as a parameter
    # to format the query
    for d in dynamic_variables:
        result_variables.append(_load_dynamic_variable(connection, d, result_variables))

    return result_variables


def _update_variables(variables, overrides):
    # type: (List[Variable], Dict[str, str]) -> List[Variable]
    """
    Updates variables with values from `overrides` dictionary
    and returns a new variables list.
    """
    variables_dict = {v.name: v for v in variables}
    overrides_dict = {n: Variable(n, v) for n, v in overrides.items()}
    variables_dict.update(overrides_dict)

    return list(variables_dict.values())


def _load_dynamic_variable(connection, variable, static_variables):
    # type: (psycopg2.extensions.connection, DynamicVariable, List[Variable]) -> Variable
    """
    Gets value of a dynamic variable from a database and returns a variable instance.


    Dynamic variables can contain static variables in queries:
        DynamicVariable(name='user_id', query='SELECT * FROM users WHERE id={user_id}')

    That's why the function expects static_variables list as an argument.
    """
    logger.debug(
        'Loading variable=%s static_variables=%s', variable.name, static_variables
    )
    literals = {v.name: v.value for v in static_variables}
    formatted_query = db.format_sql(connection, variable.query, literals=literals)
    rows = db.execute(connection, formatted_query)
    rows = cast(List[List[Any]], rows)
    value = [r[0] for r in rows]

    return Variable(variable.name, value)
