import logging
from typing import Dict, TYPE_CHECKING, List, Any, cast, Set, Optional

from panamax.manifest import Table
from panamax import db
from panamax.utils import tcolors


if TYPE_CHECKING:
    import psycopg2.extensions


logger = logging.getLogger(__name__)


def get_tables_related_to(
    connection,  # type: psycopg2.extensions.connection
    table_name,  # type: str
    column,  # type: str
):  # type: (...) -> List[Dict[str, Any]]
    """
    Returns a python dictionary containing information about all
    tables related to a given table.

    For example, there are two tables:
      - `users`
      - `cars`

    and users table has a FK to cars: `car_id`.

    Result of the query ran by the function will be:

        +----------+-----------+
        |table_name|column_name|
        |----------+-----------+
        |users     |car_id     |
        +----------+-----------+
    """

    query = '''
    SELECT tc.table_name,
           kcu.column_name
    FROM information_schema.table_constraints tc
       JOIN information_schema.key_column_usage kcu ON tc.constraint_name = kcu.constraint_name
       JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_name = tc.constraint_name
    WHERE constraint_type = 'FOREIGN KEY'
       AND ccu.table_name=%s
       AND ccu.column_name=%s;
    '''

    params = (table_name, column)

    rows = db.execute(connection, query, params, dictionary_cursor=True)
    rows = cast(List[Dict[str, Any]], rows)

    return rows


def get_fk_tables(
    connection,  # type: psycopg2.extensions.connection
    table_name,  # type: str
):  # type: (...) -> List[Dict[str, Any]]
    """
    Returns tables to which `table_name` has foreign keys in the following format:

    [
        {
            'table_name': 'users',
            'column_name': 'company_id',
            'foreign_column_name': 'id',
            'foreign_table_name': 'companies',
        }
        ...
    ]
    """
    query = '''
    SELECT
        tc.table_name,
        kcu.column_name,
        ccu.table_name AS foreign_table_name,
        ccu.column_name AS foreign_column_name
    FROM
        information_schema.table_constraints AS tc
        JOIN information_schema.key_column_usage AS kcu
        ON tc.constraint_name = kcu.constraint_name
        AND tc.table_schema = kcu.table_schema
        JOIN information_schema.constraint_column_usage AS ccu
        ON ccu.constraint_name = tc.constraint_name
        AND ccu.table_schema = tc.table_schema
    WHERE tc.constraint_type = 'FOREIGN KEY' AND tc.table_name=%s;
    '''

    params = (table_name,)

    rows = db.execute(connection, query, params, dictionary_cursor=True)
    rows = cast(List[Dict[str, Any]], rows)

    return rows


def compare_tables_with_database(
    connection,  # type: psycopg2.extensions.connection
    tables,  # type: List[Table]
):  # type: (...) -> None
    all_table_names = [t.name for t in tables]

    db_table_names = [t['table_name'] for t in db.get_tables(connection)]

    tables_not_in_manifest = sorted(set(db_table_names) - set(all_table_names))
    count = len(tables_not_in_manifest)

    tables_str = ''
    for t in tables_not_in_manifest:
        tables_str = '{}\n  - {}'.format(tables_str, t)

    print(
        '%sTables in PostgreSQL but not in the manifest (%s)%s%s\n'
        % (tcolors.YELLOW, count, tcolors.ENDC, tables_str)
    )


def _build_table_graph(
    connection,  # type: psycopg2.extensions.connection
    table_name,  # type: str
    graph=None,  # type: Optional[Dict[str, Dict[str, str]]]
    visited=None,  # type: Optional[Set[str]]
):
    # type: (...) -> Dict[str, Dict[str, str]]
    if graph is None:
        logger.debug('Building dependency graph for table=%s', table_name)
        graph = {}

    if visited is None:
        visited = set()

    visited.add(table_name)

    relations = get_tables_related_to(connection, table_name, column='id')

    for rel in relations:
        referencing_table = rel['table_name']
        referencing_column = rel['column_name']

        if referencing_table == table_name:
            # doesn't support self-referencing
            continue

        if referencing_table not in graph:
            graph[referencing_table] = {}

        graph[referencing_table][table_name] = referencing_column

        if referencing_table not in visited:
            _build_table_graph(
                connection, referencing_table, graph=graph, visited=visited,
            )

    return graph


def _bfs_paths(graph, start, target):
    # type: (Dict[str, Dict[str, str]], str, str) -> List[List[str]]
    """
    Returns paths between two nodes of a graph.
    """
    queue = [(n, [start]) for n in graph.get(start, [])]
    visited = set()  # type: Set[str]
    paths_to_target = []  # type: List[List[str]]

    while queue:
        next_node, path = queue.pop(0)

        if next_node == target:
            path.append(next_node)
            paths_to_target.append(path)

        if next_node in visited:
            continue

        visited.add(next_node)

        to_visit = set(graph.get(next_node, [])) - visited
        queue += [(n, path + [next_node]) for n in to_visit]

    return paths_to_target


def _direct_path(graph, start, target):
    # type: (Dict[str, Dict[str, str]], str, str) -> Optional[List[str]]
    """
    If between two nodes exists a path with length 2,
    return only this path, otherwise return all paths.

    For DB tables it means that when there is a direct path between
    two tables, `start` table has a FK to `target` and we can just skip
    all other paths.
    """
    if target in graph[start]:
        return [start, target]

    return None


def _build_query_from_path(connection, graph, path):
    # type: (psycopg2.extensions.connection, Dict[str, Dict[str, str]], List[str]) -> str
    query_parts = [
        db.format_sql(
            connection,
            'WITH ids AS ({{query}}) SELECT {table}.* FROM {table}',
            identifiers={'table': path[0]},
        )
    ]

    # we don't need the first and the last parts of the path
    # (the source table and the target table)
    for index, table in enumerate(path[1:-1], 1):
        # build query using inner joins for intermediate path parts
        query = 'INNER JOIN {table} ON {previous_table}.{referencing_column}={table}.id'
        identifiers = {
            'previous_table': path[index - 1],
            'table': table,
            'referencing_column': graph[path[index - 1]][table],
        }
        query_parts.append(db.format_sql(connection, query, identifiers=identifiers))

    # now we can add the last part of the query:
    # WHERE clause with a referencing column
    # from the last table in the path to the target table
    query = 'WHERE {previous_table}.{referencing_column} IN (SELECT id FROM ids)'
    identifiers = {
        'previous_table': path[-2],
        'referencing_column': graph[path[-2]][path[-1]],
    }
    query_parts.append(db.format_sql(connection, query, identifiers=identifiers))

    return ' '.join(query_parts)


def follow_relations(
    connection,  # type: psycopg2.extensions.connection
    tables,  # type: List[Table]
):  # type: (...) -> List[Table]

    # build a list of table names from the manifest.
    # If a table is in the manifest, we shouldn't build queries to extract it
    # automatically, we assume that logic to extract it was overriden with manual queries.
    known_table_names = [t.name for t in tables]

    related = []  # type: List[Table]

    for t in tables:
        if t.follow_relations:
            related += _related_tables(connection, t, known_tables=known_table_names)
            related += _referencing_to_tables(connection, t)

    return related


def _referencing_to_tables(connection, table):
    # type: (psycopg2.extensions.connection, Table) -> List[Table]
    """
    Returns list of tables (List[Table]) to which
    the original `table` is referencing
    """
    referencing_to = []  # type: List[Table]

    for fk_relation in get_fk_tables(connection, table.name):
        query = '''
            WITH orig_query AS ({query})

            SELECT {foreign_table_name}.* FROM {foreign_table_name}
            WHERE {foreign_table_name}.{foreign_column_name} IN (
                SELECT {table_name}.{column_name} FROM {table_name}
                INNER JOIN orig_query ON orig_query.id = {table_name}.id
            )
        '''.format(
            foreign_column_name=fk_relation['foreign_column_name'],
            foreign_table_name=fk_relation['foreign_table_name'],
            table_name=fk_relation['table_name'],
            column_name=fk_relation['column_name'],
            query=table.query,
        )
        # type ignore below: ignore "too few arguments for Table"
        referencing_to.append(Table(fk_relation['foreign_table_name'], query))  # type: ignore

    return referencing_to


def _related_tables(connection, table, known_tables):
    # type: (psycopg2.extensions.connection, Table, List[str]) -> List[Table]
    """
    Returns list of tables which are referencing to the original `table`.

    Args:
        connection: psycopg2.extensions.connection
        table: str - table to which we are tracking relations
        known_tables: List[str] - tables from the manifest. If a related table
                                  is in the list, the function skips it and doesn't
                                  build queries to extract it automatically.
    """
    logger.debug('Following relations for table=%s', table.name)

    related = []  # type: List[Table]

    graph = _build_table_graph(connection, table.name)

    for related_table in graph:
        if related_table in known_tables:
            # if it is already in the manifest,
            # don't do anything automatically
            logger.debug("table=%s is in known_tables list, skipping", table.name)
            continue

        logger.debug('table=%s is referincing table=%s', related_table, table.name)

        # try to find a direct path between two tables
        # (it exists if `related_table` has a FK to `table.name`)
        direct_path = _direct_path(graph, start=related_table, target=table.name)
        if direct_path:
            paths = [direct_path]
        else:
            paths = _bfs_paths(graph, start=related_table, target=table.name)

        for p in paths:
            query = _build_query_from_path(connection, graph, p).format(
                query=table.query
            )
            logger.debug('built a query to get table=%s query=%s', related_table, query)
            related.append(
                Table(
                    related_table,
                    query=query,
                    post_import_actions=[],
                    follow_relations=False,
                )
            )

    return related
