import argparse

from panamax.analyze import relations
from panamax import manifest, utils, db


def main():
    parser = argparse.ArgumentParser(description='cool tool')
    parser.add_argument('-f', '--file', required=True, help='Manifest filename')
    parser.add_argument('-a', '--host', default='localhost', help='database host')
    parser.add_argument('-p', '--port', default=5432, help='database port')
    parser.add_argument('-u', '--user', help='database username')
    parser.add_argument('-d', '--database', required=True, help='database name')
    parser.add_argument('-l', '--loglevel', default='info', help='logging level')
    args = parser.parse_args()

    utils.setup_logging(level_name=args.loglevel)
    password = utils.get_password()

    connection = db.connect(args.database, args.host, args.port, args.user, password,)

    m = manifest.load(args.file)
    tables = m[1] + relations.follow_relations(connection, m[1])
    relations.compare_tables_with_database(connection, tables)
    connection.close()


if __name__ == '__main__':
    main()
