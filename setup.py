#!/usr/bin/env python
import os
import re
from setuptools import setup, find_packages


def get_version(package):
    """
    Returns version of a package (`__version__ = "x.y.z"` in `__init__.py`).
    """
    init_py = open(os.path.join(package, '__init__.py')).read()

    return re.match("__version__ = ['\"]([^'\"]+)['\"]", init_py).group(1)


name = 'panamax'
version = get_version(name)

setup(
    name=name,
    version=version,
    description='description',
    author='author',
    author_email='author@email',
    url='url',
    packages=find_packages(exclude=['panamax.tests']),
    install_requires=['toml', 'psycopg2-binary'],
    entry_points={'console_scripts': ['{name}={name}.cli:main'.format(name=name),],},
)
