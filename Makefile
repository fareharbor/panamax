.PHONY: test tests


test:tests


tests:
	circleci local execute --job mypy
	circleci local execute --job black
	circleci local execute --job flake8
	circleci local execute --job tests-py2
	circleci local execute --job tests-py3


docker/tests/integration/run:
	docker-compose -f tests/integration/docker-compose.yml run panamax_test


docker/tests/integration/build:
	docker-compose -f tests/integration/docker-compose.yml build
