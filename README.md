# Panamax
[![CircleCI](https://circleci.com/bb/fareharbor/panamax/tree/master.svg?style=svg)](https://circleci.com/bb/fareharbor/panamax/tree/master)

A tool to dump subset of a database.

- [Changelog](/CHANGELOG.md)

## How to use

First, you have to prepare a manifest file. Manifest describes how to dump part
of a database and should be in [TOML](https://github.com/toml-lang/toml) format:

```toml
[variables]
user_id = 1


[dynamic_variables]
group_ids = "SELECT id FROM groups WHERE name ILIKE '%admin%'"
# panamax converts group_ids to ARRAY[...]


[[tables]]
table = "users"
query = """
    SELECT users.* FROM users 
    WHERE users.id = {user_id} 
        OR users.group_id = ANY({group_ids})"""
post_import_actions = ["UPDATE users SET name = 'sanitized'"]
```

### Variables

Static variables you can use in the `tables` section in any table query:

```toml
[variables]
user_id = 1

[[tables]]
table = "users"
# python's format syntax, doesn't allow spaces around variable name
query = "SELECT users.* FROM users WHERE users.id = {user_id}"
```

### Dynamic variables

Dynamic variables allow you to specify a SQL query to get list of variables: 

```toml
[dynamic_variables]
group_ids = "SELECT id FROM groups WHERE name ILIKE '%admin%'"

[[tables]]
table = "users"
query = """
    SELECT users.* FROM users 
    WHERE users.group_id = ANY({group_ids})"""
    # psycopg2 automatically inserts group_ids as an ARRAY[...]
```

Dynamic variables must be always used as a **list** in all queries.

You can use variables in dynamic variable queries, but not in other static
variables.

### Post import actions

Optionally, you can specify post import actions: list of SQL queries to run
after the dumped table is loaded.

```toml
[[tables]]
table = "users"
query = "SELECT 1"
post_import_actions = ["UPDATE {table} SET name = 'sanitized'"]
```

Note that instead of updating `users` table you must use `{table}` variable,
and Panamax will automatically insert the table name.

Variables are allowed in post import queries.

### Relations

Panamax can automatically add tables referencing to another table to your
manifest:

```
[[tables]]
table = "users"
query = "SELECT * FROM users WHERE id = 5"
follow_relations = true
```

All tables with FK to `users` will be automatically added to the manifest.
Panamax builds SQL queries to select data from related tables using `table.query` as a
value for the referencing columns.


## Library API

```python
import panamax


variables = [
    panamax.Variable(name='user_id', value=5),
]

users_query = 'SELECT * FROM {table} WHERE id = {user_id}'
tables = [
    panamax.Table(name='users', query=users_query),
]

manifests = [(variables, tables)]

# connection = psycopg2...

panamax.dump(
    connection, 
    manifests, 
    output_filename='/tmp/dump.sql.gz',
    gzip_compress=True,
)

```


## CLI

```shell
python -m panamax -f manifest.toml \
                  -f another_manifest.toml \
                  -a hostname \
                  -u username \
                  -d database \
                  -o dump.sql
```

## Development

### Run tests

First, you have to install CircleCI [locally](https://circleci.com/docs/2.0/local-cli/):

```shell
brew install circleci
```

Run all tests:

```shell
make tests
```

Run only python2 tests:

```shell
circleci local execute --job tests-py2
```

See all job names in `.circleci/config.yml`.

You can use `tox` to run tests, but tests require a locally running PostgreSQL
instance and a few environment variables:

```
POSTGRES_HOST: "localhost"
POSTGRES_PORT: 5432
POSTGRES_USER: "panamax_tests"
POSTGRES_DB: "panamax_tests"
POSTGRES_PASSWORD: "password"
```

### Run integration tests

```shell
make run_integration_tests
```
