# Changelog

## [0.6.0]
- Better error handling during database operations.
- Updated documentation (`README.md`) about post import actions:
  you musg use `{table}` instead of a table name.

## [0.5.2]
- A `DynamicVariable` can now be used inside another `DynamicVariable` as these
  are loaded in order.

## [0.5.1]
- `post_import_actions` are now applied to the temporary table before data is
  extracted into the dump file

## [0.5.0]
- Removed `relation_id` from `tables` elements. Now Panamax uses table's query
  to retreive related rows.
- Now it is possible to use variables inside dynamic variable queries.
- Supported variables in `post_import_actions`.

## [0.4.0]
- Improved performance: if there is a direct FK from a table to another table,
  Panamax doesn't use joins anymore and uses FK field to dump part of the table.

## [0.3.1]
- Poetry was removed in favor of setup.py. Requirements files were added to
  accommodate this and changes to `Dockerfile`, `tox` and `pyproject.toml` were
  done accordingly.

## [0.3.0]
- Now Panamax uses `psycopg2.sql` module to compose SQL queries. It changed the
  way how you should use dynamic variables (it automatically adds `ARRAY[...]`,
  etc.), see README.md for examples.

## [0.2.0]

- Fixed documentation in README.md
- Added dynamic variables support.
- Added a new CLI command: paramax.analyze to compare tables between DB and a
  manifest file
- Added two new optional parameters to panamax.Table (and toml [table]):
  `follow_relations` and `relation_id` to automatically add all tables with a FK to
  the source table to the manifest

## [0.1.0]

- Initial release
